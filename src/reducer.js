import { xxxFormatPrice } from './utilities';

const initState = {
  products: {
    data: {
      byId: [],
      byHash: {}
    },
    loading: false,
  },
  cart: {
    data: {
      byId: [],
      byHash: {}
    },
    total: 0,
    loading: false,
  }
}

const shoppyReducer = (state = initState, { type, payload }) => {
  switch (type) {

    case 'GET_PRODUCTS': {
      return { ...state, products: { ...state.products, data: payload } }
    }

    case 'INCREMENT_CART_ITEM_QTY': {
      const newQty = payload.qty;
      const _newCartItem = { [payload.product.id]: { ...state.cart.data.byHash[payload.product.id], qty: newQty } }
      const _newCartHash = { ...state.cart.data.byHash, ..._newCartItem };

      return {
        ...state,
        cart: {
          ...state.cart,
          data: {
            ...state.cart.data,
            byHash: _newCartHash,
          }
        }
      }
    }

    case 'DECREMENT_CART_ITEM_QTY': {
      const newQty = payload.qty;
      const _newCartItem = { [payload.product.id]: { ...state.cart.data.byHash[payload.product.id], qty: newQty } }
      const _newCartHash = { ...state.cart.data.byHash, ..._newCartItem };

      let newState = {
        ...state,
        cart: {
          ...state.cart,
          data: {
            ...state.cart.data,
            byHash: _newCartHash,
          }
        }
      }

      return newState;
    }

    case 'ADD_TO_CART': {

      const newCartIds = [...state.cart.data.byId, payload.id];
      const newCartHashItem = { [payload.id]: { product: { ...payload }, qty: 1 } }
      const newCartHash = { ...state.cart.data.byHash, ...newCartHashItem }
      const newProductHashItem = { [payload.id]: { ...state.products.data.byHash[payload.id], inCart: true } }
      const newProductHash = { ...state.products.data.byHash, ...newProductHashItem }

      return ({
        ...state,
        cart: {
          ...state.cart,
          data: {
            ...state.cart.data,
            byId: newCartIds,
            byHash: newCartHash,
          }
        },
        products: {
          ...state.products,
          data: {
            ...state.products.data,
            byHash: newProductHash,
          }
        }
      })
    }

    case 'REMOVE_FROM_CART': {

      const newCartHash = { ...state.cart.data.byHash }
      delete newCartHash[payload.id];

      let newCartIds = state.cart.data.byId.filter(id => id !== payload.id);
      // update product to be available again 
      let newProduct = { [payload.id]: { ...state.products.data.byHash[payload.id], inCart: false } }
      let newProductHash = { ...state.products.data.byHash, ...newProduct }

      return ({
        ...state,
        cart: {
          ...state.cart,
          data: {
            ...state.cart.data,
            byHash: newCartHash,
            byId: newCartIds,
          }
        },
        products: {
          ...state.products,
          data: {
            ...state.products.data,
            byHash: newProductHash,
          }
        }
      })
    }

    case 'CALC_CART_TOTAL': {

      let total = 0;
      state.cart.data.byId.map(id => {
        total += state.cart.data.byHash[id].product.price * state.cart.data.byHash[id].qty
        return id;
      })

      return ({
        ...state,
        cart: {
          ...state.cart,
          total: xxxFormatPrice(total)
        }
      });
    }

    default:
      return state
  }
}

export default shoppyReducer;