import { products } from './data';

// might simulate server delay here... 
export const getProducts = () => {
  return {
    type: 'GET_PRODUCTS',
    payload: products
  }
};

export const addToCart = (product) => {

  return {
    type: 'ADD_TO_CART',
    payload: product,
  }
}

export const removeFromCart = (product) => {

  return {
    type: 'REMOVE_FROM_CART',
    payload: product,
  }
}

// operator arg = increment or decrement qty 
export const handleCartQtyChange = (cartItem, operator) => {

  // current qty value
  const { qty } = cartItem;

  // handle increase 
  if (operator === 'increment') {

    return ({
      type: 'INCREMENT_CART_ITEM_QTY',
      payload: {
        ...cartItem,
        qty: parseInt(qty) + 1,
      }
    })
  }


  // handle decrease
  if (operator === 'decrement') {
    return ({
      type: 'DECREMENT_CART_ITEM_QTY',
      payload: {
        ...cartItem,
        qty: parseInt(qty) - 1,
      }
    })
  }

}

export const calcCartTotal = () => {
  return {
    type: 'CALC_CART_TOTAL',
  }
}