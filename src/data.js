export const products = {
  byId: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
  byHash: {
    "1": {
      id: 1,
      title: "Butter",
      desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Expedita doloribus sint dolor, laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis facere hic rerum magnam ipsum laudantium.",
      price: 550, // declaring prices in cents for ease
      inCart: false,
    },
    "2": {
      id: 2,
      title: "Bread",
      desc: "Expedita doloribus sint dolor, laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis.",
      price: 200, // declaring prices in cents for ease
      inCart: false,
    },
    "3": {
      id: 3,
      title: "Milk",
      desc: "Laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis.",
      price: 259, // declaring prices in cents for ease
      inCart: false,
    },
    "4": {
      id: 4,
      title: "Orange Juice",
      desc: "Expedita doloribus sint dolor, laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis.",
      price: 399, // declaring prices in cents for ease
      inCart: false,
    },
    "5": {
      id: 5,
      title: "Cereal",
      desc: "Expedita doloribus sint dolor, laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis.",
      price: 659, // declaring prices in cents for ease
      inCart: false,
    },
    "6": {
      id: 6,
      title: "Ice Cream",
      desc: "Expedita doloribus sint dolor, laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis.",
      price: 349, // declaring prices in cents for ease
      inCart: false,
    },
    "7": {
      id: 7,
      title: "Pasta",
      desc: "Expedita doloribus sint dolor, laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis.",
      price: 125, // declaring prices in cents for ease
      inCart: false,
    },
    "8": {
      id: 8,
      title: "Toothpaste",
      desc: "Expedita doloribus sint dolor, laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis.",
      price: 915, // declaring prices in cents for ease
      inCart: false,
    },
    "9": {
      id: 9,
      title: "Eggs",
      desc: "Expedita doloribus sint dolor, laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis.",
      price: 190, // declaring prices in cents for ease
      inCart: false,
    },
    "10": {
      id: 10,
      title: "Cheese",
      desc: "Expedita doloribus sint dolor, laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis.",
      price: 250, // declaring prices in cents for ease
      inCart: false,
    },
    "11": {
      id: 11,
      title: "Ketchup",
      desc: "Expedita doloribus sint dolor, laboriosam sunt aperiam placeat illum quisquam, non et voluptate mollitia eligendi perferendis.",
      price: 399, // declaring prices in cents for ease
      inCart: false,
    },
  }
}
