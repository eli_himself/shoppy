import React from 'react'

const Nav = () => {
  return (
    <nav className="navbar navbar-dark bg-dark mb-5 d-flex align-items-center justify-content-between">
      <h4 className="text-white m-0"><i className="fas fa-certificate"></i> shoppy</h4>

      <ul className="navbar-nav mr-auto ml-5">
        <li className="nav-item active">
          <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
        </li>
      </ul>

      <p className="m-0 text-white">Welcome! <span className="font-weight-bold">John Smith</span></p>
    </nav>
  )
}

export default Nav
