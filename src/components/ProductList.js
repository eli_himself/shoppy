import React, { Component } from 'react'
import Product from './Product';
import { connect } from 'react-redux';
import { getProducts } from '../actions';

class ProductList extends Component {

  componentDidMount() {
    this.props.getProducts();
  }

  render() {

    const { byId, byHash } = this.props.products;

    return (
      <div className="row">

        {byId.map(id => <div key={id} className="col-sm-6 col-lg-4"><Product inCart={byHash[id].inCart} product={byHash[id]} /></div>)}

      </div>
    )

  }
}

const mapStateToProps = state => ({
  products: state.products.data,
  loading: state.products.loading,
})

export default connect(mapStateToProps, { getProducts })(ProductList);