import React from 'react'
import { xxxTrimString, xxxFormatPrice } from '../utilities';
import { connect } from 'react-redux';
import { addToCart } from '../actions';

const Product = (props) => {

  const { title, desc, price } = props.product;

  const renderAddToCartBtn = () => {
    const { inCart } = props;

    // handle clase when item is in cart
    if (inCart) {
      return (
        <button
          className="btn btn-dark btn-sm"
          disabled
        >
          <i className="fas fa-check"></i> Item in cart
        </button>
      )
    }

    // item not in cart 
    return (
      <button
        className="btn btn-dark btn-sm"
        onClick={handleAddToCart}
      >
        <i className="fas fa-plus"></i> Add to cart
      </button>
    );

  }

  const handleAddToCart = () => {
    props.addToCart(props.product);
  }

  return (
    <div className="card mb-4 shadow">
      <img className="card-img-top" src="https://baconmockup.com/320/200" alt="bacon!" />

      <div className="card-body">

        <h5>{title}</h5>
        <p className="text-muted">{xxxTrimString(desc, 45)}</p>

        <div className="d-flex align-items-center">
          <div className="font-weight-bold mr-3">€{xxxFormatPrice(price)}</div>
          {renderAddToCartBtn()}
        </div>
      </div>
    </div>
  )

}

export default connect(null, { addToCart })(Product);