import React from 'react'
import CartItem from './CartItem';
import { connect } from 'react-redux';
import { calcCartTotal } from '../actions';

const Cart = (props) => {

  const { cartItems: { byId, byHash }, total } = props;

  const renderItems = () => {

    // handle no items in cart case
    if (byId.length === 0) {
      return (
        <div className="text-muted">Your cart is empty.</div>
      )
    }

    // handle loading case ?

    // handle items present case
    if (byId.length > 0) {
      return byId.map(id => <CartItem key={id} cartItem={byHash[id]} qty={byHash[id].qty} />)
    }
  }

  const disableCheckoutBtn = () => {

    // if no items in cart case
    if (byId.length === 0) {
      return true;
    }

    // default case
    return false;
  }

  // calc total
  props.calcCartTotal();

  return (
    <div className="card p-3 shadow">
      <h2 className="mb-4">Cart</h2>

      <div className="list-group mb-4">

        {renderItems()}

      </div>

      <div className="d-flex align-items-center">
        <div className="mr-3">
          <h3 className="m-0"><span>€{total}</span></h3>
        </div>
        <button className="btn btn-success btn-lg" disabled={disableCheckoutBtn()}><i className="fas fa-shopping-cart"></i> Checkout</button>
      </div>

    </div>
  )
}

const mapStateToProps = state => ({
  cartItems: state.cart.data,
  loading: state.cart.loading,
  total: state.cart.total,
});

export default connect(mapStateToProps, { calcCartTotal })(Cart);
