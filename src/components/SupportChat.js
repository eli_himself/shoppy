import React, { Component, Fragment } from 'react'

const triggerStyle = {
  position: 'fixed',
  bottom: '30px',
  right: '30px',
  zIndex: '100',
}

export default class SupportChat extends Component {
  render() {
    return (
      <Fragment>
        <button
          style={triggerStyle}
          className="btn btn-primary btn-lg shadow text-white font-weight-bold"
          data-toggle="modal"
          data-target="#exampleModal">
          <i className="far fa-comments"></i> Need Help?
        </button>

        <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel"><i className="far fa-comments"></i> Customer Support Chat</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div className="modal-body pt-4">

                <div style={{ maxWidth: "fit-content" }} className="p-3 bg-light rounded shadow mb-3 text-right float-right">
                  <h6 className="font-weight-bold"><i className="fas fa-user-tie"></i> Lisa - <small className="text-muted font-weight-light">Support Agent</small></h6>
                  <div>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit ea quis ex doloremque perferendis
                  </div>
                </div>

                <div style={{ maxWidth: "fit-content" }} className="p-3 bg-dark rounded shadow mb-3 text-white float-left">
                  <h6 className="font-weight-bold"><i className="fas fa-user"></i> You</h6>
                  <div>
                    Lorem ipsum dolor sit amet.
                  </div>
                </div>

                <div style={{ maxWidth: "fit-content" }} className="p-3 bg-light rounded shadow mb-3 text-right float-right">
                  <h6 className="font-weight-bold"><i className="fas fa-user-tie"></i> Lisa - <small className="text-muted font-weight-light">Support Agent</small></h6>
                  <div>
                    Reprehenderit ea quis ex doloremque perferendis
                  </div>
                </div>

              </div>

              <div className="modal-footer">
                <textarea className="form-control" cols="30" rows="1" placeholder="Type message..."></textarea>
                <button type="button" className="btn btn-primary"><i className="fas fa-paper-plane"></i></button>
              </div>
            </div>
          </div>
        </div>

      </Fragment>
    )
  }
}
