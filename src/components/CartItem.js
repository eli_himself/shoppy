import React from 'react'
import { handleCartQtyChange, removeFromCart } from '../actions';
import { connect } from 'react-redux';

const CartItem = (props) => {

  const handleDecrement = () => {
    props.handleCartQtyChange(props.cartItem, 'decrement');

    // remove from cart?
    if (parseInt(props.cartItem.qty) === 1) {
      props.removeFromCart(props.cartItem.product)
    }
  }

  const { product: { title } } = props.cartItem;

  return (
    <div className="list-group-item">
      <h6 className="d-flex align-items-center">{title}</h6>
      <div className="d-flex align-items-center">
        <span className="mr-2">Qty:</span>
        <button
          className="btn btn-sm btn-light mr-2"
          onClick={handleDecrement}
        >
          <i className="fas fa-minus"></i>
        </button>
        <span className="mr-2 font-weight-bold">{props.qty}</span>
        <button
          className="btn btn-sm btn-dark"
          onClick={() => props.handleCartQtyChange(props.cartItem, 'increment')}
        >
          <i className="fas fa-plus"></i>
        </button>
      </div>
    </div>
  )
}


export default connect(null, { handleCartQtyChange, removeFromCart })(CartItem);