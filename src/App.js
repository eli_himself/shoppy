import React from 'react';
import './App.css';
import ProductList from './components/ProductList';
import Cart from './components/Cart';
import SupportChat from './components/SupportChat';
import Nav from './components/Nav';

function App() {
  return (
    <div className="App">

      <SupportChat />

      <Nav />


      <main className="container mb-5">
        <div className="row">

          <div className="col-md-8">
            <ProductList />
          </div>

          <div className="col-md-4">
            <Cart />
          </div>

        </div>
      </main>

    </div>
  );
}

export default App;
