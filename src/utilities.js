// used for descriptions
export const xxxTrimString = (string, length = 20) => string.substring(0, length) + '...';

export const xxxFormatPrice = (price) => (price / 100).toFixed(2);